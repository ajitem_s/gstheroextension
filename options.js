let saveButton = document.getElementById('save');

chrome.storage.sync.get('username', function(data) {
    document.getElementById('username').value = data.username
});

chrome.storage.sync.get('password', function(data) {
    document.getElementById('password').value = data.password
});

saveButton.onclick = function(element) {
    u = document.getElementById('username').value
    p = document.getElementById('password').value

    chrome.storage.sync.set({username: u, password: p}, function() {
        console.log(u, p);
    });
}